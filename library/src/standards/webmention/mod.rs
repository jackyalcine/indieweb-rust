use serde::{Deserialize, Serialize};
use std::cmp::PartialEq;
use url::Url;

/// Resolves the Webmention endpoints for the provided URL.
///
/// # Examples
///
/// ```
/// use indieweb::standards::webmention::endpoint_for;
/// use indieweb::http::{Client, reqwest::Client as HttpClient};
/// use url::Url;
///
/// # tokio_test::block_on(async {
/// let the_url: Url = "https://news.indieweb.org".parse().unwrap();
/// let client = HttpClient::default();
/// assert_eq!(
///     endpoint_for(&client, &the_url).await,
///     "https://news.indieweb.org/en/webmention"
///         .parse()
///         .map_err(indieweb::Error::Url),
///     "found the Webmention endpoint");
/// # })
/// ```
#[tracing::instrument(skip(client))]
pub async fn endpoint_for(
    client: &impl crate::http::Client,
    url: &Url,
) -> Result<url::Url, crate::Error> {
    let rels = crate::algorithms::link_rel::for_url(client, url, &["webmention"])
        .await?
        .get("webmention")
        .cloned()
        .unwrap_or_default();

    if let Some(endpoint_url) = rels.first().cloned() {
        tracing::debug!(
            rels = format!("{:?}", rels),
            url = format!("{endpoint_url}"),
            "Found the relations for Webmention; selecting the first URL"
        );

        Ok(endpoint_url)
    } else {
        tracing::trace!(
            url = format!("{}", url),
            "No Webmention endpoints were found."
        );

        Err(crate::Error::NoEndpointsFound {
            url: url.to_string(),
            rel: "webmention".to_owned(),
        })
    }
}

/// Handles the work of sending a Webmention from a [request][Request].
///
/// This uses the [request][Request] to send a Webmention. You can use
/// the [builder][Request::builder] to simplify the act of crafting it.
///
/// # Examples
/// This is what it looks like sending a Webmention that processes it synchronously.
/// ```
/// use indieweb::standards::webmention::{Request, Response, send, Status};
/// use indieweb::http::{Client, reqwest::Client as HttpClient};
/// use url::Url;
/// # use mockito::*;
///
/// # let mut server = Server::new();
/// let client = HttpClient::default();
/// # let source = format!("{}/source", server.url()).parse().unwrap();
/// # let target = format!("{}/target", server.url()).parse().unwrap();
/// # let endpoint: Url = format!("{}/endpoint", server.url()).parse().unwrap();
/// # let mock_endpoint = server.mock("POST", endpoint.path()).with_status(200).expect(1).create();
/// let public_request = Request::builder()
///     .source(source) // Any `url::Url` will do.
///     .target(target); // Same here
///
/// # tokio_test::block_on(async {
/// let public_result = send(&client, &endpoint, &public_request.build()).await;
/// assert_eq!(
///     public_result,
///     Ok(Response {
///         status: Status::Sent,
///         location: None,
///         body: None
///     })
/// );
/// # mock_endpoint.assert();
/// # })
/// ```
///
/// This is how it'd look when it's processed asynchronously pointing to a status page.
/// ```
/// use indieweb::standards::webmention::{Request, Response, send, Status};
/// use indieweb::http::{Client, reqwest::Client as HttpClient};
/// use url::Url;
/// # use mockito::*;
///
/// # let mut server = Server::new();
/// let client = HttpClient::default();
/// # let source = format!("{}/source", server.url()).parse().unwrap();
/// # let target = format!("{}/target", server.url()).parse().unwrap();
/// # let endpoint: Url = format!("{}/endpoint", server.url()).parse().unwrap();
/// # let mock_endpoint = server.mock("POST", endpoint.path())
/// #   .with_status(201)
/// #   .with_header("Location", "http://webmention.example/status")
/// #   .expect(1)
/// #   .create();
/// let public_request = Request::builder()
///     .source(source) // Any `url::Url` will do.
///     .target(target); // Same here
///
/// # tokio_test::block_on(async {
/// let public_result = send(&client, &endpoint, &public_request.build()).await;
/// assert_eq!(
///     public_result,
///     Ok(Response {
///         status: Status::Accepted,
///         location: "http://webmention.example/status".parse().ok(),
///         body: None
///     })
/// );
/// # mock_endpoint.assert();
/// # })
/// ```
///
#[tracing::instrument(skip(client))]
pub async fn send(
    client: &impl crate::http::Client,
    endpoint: &Url,
    request: &Request,
) -> Result<Response, crate::Error> {
    use std::str::FromStr;

    let local_request = request.clone();
    let mut req: http::Request<crate::http::Body> = local_request.try_into()?;
    *req.uri_mut() =
        http::Uri::from_str(endpoint.as_str()).map_err(|e| crate::Error::Http(e.into()))?;
    client.send_request(req).await.and_then(|r| r.try_into())
}

/// Represents the state of a Webmention for a client or a server.
#[derive(Clone, Debug, Serialize, Deserialize, Eq)]
#[serde(untagged, rename_all = "lowercase")]
pub enum Status {
    /// Used to highlight that a stored Webmention or a response about a Webmention has
    /// been rejected by a remote endpoint or the implementation of a server is informing a client
    /// that it has been rejected.
    Rejected,

    /// Used to highlight that an asynchronous Webmention has been accepted for processing
    /// by a remote endpoint.
    Accepted,

    /// Represents a successful synchronous Webmention.
    Sent,

    /// Reports that the sender failed to satisfy constraints for this Webmention.
    ///
    /// This value is usually in the `4xx` range.
    SenderError(u16),

    /// Reports that the receiver failed to satisfy constraints for this Webmention.
    ///
    /// This value is usually in the `5xx` range.
    ReceiverError(u16),
}

impl PartialEq for Status {
    fn eq(&self, other: &Self) -> bool {
        std::mem::discriminant(self) == std::mem::discriminant(other)
    }
}

impl Status {
    /// Determines if this status is not of an error status.
    ///
    /// # Examples
    ///
    /// ```
    /// use indieweb::standards::webmention::Status;
    ///
    /// assert!(Status::Accepted.is_ok());
    /// assert!(Status::Sent.is_ok());
    /// assert!(!Status::Rejected.is_ok());
    /// assert!(!Status::SenderError(409).is_ok());
    /// assert!(!Status::ReceiverError(503).is_ok());
    /// ```
    pub fn is_ok(&self) -> bool {
        match self {
            Status::Accepted | Status::Sent => true,
            Status::SenderError(_) | Status::ReceiverError(_) | Status::Rejected => false,
        }
    }
}

impl From<u16> for Status {
    fn from(code: u16) -> Self {
        match code {
            200 => Self::Sent,
            201 | 202 => Self::Accepted,
            400..=499 => Self::SenderError(code),
            _ => Self::ReceiverError(code),
        }
    }
}

/// Represents a incoming request to an endpoint for a Webmention.
///
/// The foundation of this request is defined at
/// <https://www.w3.org/TR/webmention/#sender-notifies-receiver-p-1> but
/// additional fields can be used to extend the functionality of this Webmention.
#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq, typed_builder::TypedBuilder)]
pub struct Request {
    /// The URL of the resource that's sending this Webmention.
    ///
    /// See <https://www.w3.org/TR/webmention/#sender-notifies-receiver-p-1> for more information.
    pub source: Url,

    /// The URL of the resource that's receiving this Webmention.
    ///
    /// See <https://www.w3.org/TR/webmention/#sender-notifies-receiver-p-1> for more information.
    pub target: Url,

    /// Parameters for private Webmention support.
    ///
    /// See <https://indieweb.org/Private-Webmention> for more information.
    #[serde(skip_serializing_if = "Option::is_none", flatten)]
    #[builder(default)]
    pub private: Option<PrivateRequest>,

    /// A list of URLs that can be used for vouching this URL.
    ///
    /// This is part of the sending flow described at <https://indieweb.org/Vouch#Sending>.
    ///
    /// See <https://indieweb.org/Vouch> for more information.
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub vouch: Option<Vec<String>>,

    /// A URL to send information about when this Webmention has been processed by a receiver.
    ///
    /// See <https://indieweb.org/Webmention-brainstorming#Asynchronous_status_notification> for
    /// more information.
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub callback: Option<url::Url>,

    /// A token to use to authorize this request when sending a Webmention.
    #[serde(skip)]
    #[builder(default)]
    pub token: Option<String>,
}

impl TryInto<http::Request<crate::http::Body>> for Request {
    type Error = crate::Error;

    fn try_into(mut self) -> Result<http::Request<crate::http::Body>, Self::Error> {
        let mut request_builder = http::Request::builder()
            .method("POST")
            .header(
                ::http::header::ACCEPT,
                "text/plain; q=0.8, text/html, application/json; q=0.8, application/mf2+json; q=0.9, *.*; q=0.7",
            )
            .header(
                ::http::header::CONTENT_TYPE,
                crate::http::CONTENT_TYPE_FORM_URLENCODED,
            );

        if let Some(token) = self.token.take() {
            request_builder =
                request_builder.header(::http::header::AUTHORIZATION, format!("Bearer {}", token));
        }

        let req_qs =
            serde_qs::to_string(&self).map(|s| crate::http::Body::Bytes(s.into_bytes()))?;
        request_builder.body(req_qs).map_err(crate::Error::Http)
    }
}

impl Default for Request {
    fn default() -> Self {
        Self {
            source: "urn:indieweb:invalid-source".parse().unwrap(),
            target: "urn:indieweb:invalid-target".parse().unwrap(),
            private: None,
            vouch: None,
            callback: None,
            token: None,
        }
    }
}

/// Represents parameters to use when sending a private Webmention.
/// See <https://indieweb.org/Private-Webmention> for more information.
#[derive(Clone, Default, Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct PrivateRequest {
    /// The code to exchange for a private Webmention.
    ///
    /// See <https://indieweb.org/Private-Webmention#Sending_the_Webmention> for more information.
    #[serde(skip_serializing_if = "String::is_empty")]
    pub code: String,

    /// The realm to exchange for a private Webmention.
    ///
    /// See <https://indieweb.org/Private-Webmention#Sending_the_Webmention>
    #[serde(skip_serializing_if = "Option::is_none")]
    pub realm: Option<String>,
}

/// Represents a incoming response from an endpoint for a Webmention.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Response {
    /// The resulting status of a Webmention.
    pub status: Status,

    /// A optional URL presenting the result of this URL. It can also be used
    /// at time to show status information or acceptance information.
    pub location: Option<Url>,

    /// A optional body representing the immediate body of the response.
    pub body: Option<String>,
}

impl PartialEq for Response {
    fn eq(&self, other: &Self) -> bool {
        self.status == other.status && self.location == other.location && self.body == other.body
    }
}

impl TryFrom<http::Response<crate::http::Body>> for Response {
    type Error = crate::Error;

    fn try_from(resp: http::Response<crate::http::Body>) -> Result<Self, Self::Error> {
        let locations = resp.headers().get_all("location");
        let status = resp.status();

        let location = locations
            .into_iter()
            .cloned()
            .filter(|_| Status::from(status.as_u16()) == Status::Accepted)
            .filter_map(|v| v.to_str().ok().map(ToString::to_string))
            .collect::<Vec<_>>()
            .first()
            .filter(|l| !l.is_empty())
            .and_then(|v| v.as_str().parse().ok());
        let body = Some(crate::http::to_string(resp)?).filter(|b| !b.is_empty());

        match status.as_u16() {
            200..=299 | 400..=499 | 500..=599 => {
                let status = match status.as_u16() {
                    201 | 202 => Status::Accepted,
                    200 | 203..=299 => Status::Sent,
                    400..=499 => Status::SenderError(status.as_u16()),
                    _ => Status::ReceiverError(status.as_u16()),
                };

                Ok(Self {
                    body,
                    location,
                    status,
                })
            }
            _ => Err(crate::Error::WebmentionUnsupportedStatusCode(
                status.as_u16(),
            )),
        }
    }
}

fn default_status() -> Status {
    Status::Sent
}

/// Represents the body (or response) of a Webmention callback.
#[derive(Deserialize, Serialize, Debug, Clone)]
#[serde(rename_all = "kebab-case")]
pub struct CallbackPayload {
    pub source: Url,
    pub target: Url,

    #[serde(skip, default = "default_status")]
    pub status: Status,

    #[serde(flatten, default)]
    pub other: std::collections::HashMap<String, serde_json::Value>,
}

mod test;
