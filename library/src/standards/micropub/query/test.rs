use crate::{
    http::CONTENT_TYPE_JSON,
    standards::micropub::{
        paging,
        query::{
            CategoryResponse, ConfigurationResponse, MatchingPropertyValuesMap, Query, Response,
            SourceQuery, SourceResponse,
        },
    },
};
use http::header::CONTENT_TYPE;
use microformats_types::{Class, KnownClass};
use oauth2::AccessToken;

use crate::{
    algorithms::ptd::Type,
    standards::micropub::{query::QueryKind, Error},
};

#[tracing_test::traced_test]
#[tokio::test]
async fn query_send() {
    let mut client = crate::test::Client::new().await;
    let token = AccessToken::new("a-bad-token".into());
    let query = crate::standards::micropub::query::Query {
        pagination: Default::default(),
        kind: QueryKind::Configuration,
    };
    let endpoint_mock = client
        .mock_server
        .mock("get", "/micropub/auth-failure")
        .match_query("q=config")
        .with_status(400)
        .with_header(CONTENT_TYPE.as_str(), CONTENT_TYPE_JSON)
        .with_body(
            serde_json::to_string(&serde_json::json!({
                "error": "invalid_request",
                "error_description": "This was a bad, bad request."
            }))
            .unwrap(),
        )
        .expect(1)
        .create();

    let endpoint = format!("{}/micropub/auth-failure", client.mock_server.url())
        .parse()
        .unwrap();

    let query_result = query.send(&client, &endpoint, &token).await;

    endpoint_mock.assert();

    assert_eq!(
        query_result,
        Err(Error::bad_request("This was a bad, bad request").into()),
        "reports an authorization failure"
    );
}

#[test]
fn query_from_qs_str() {
    assert_eq!(
        Ok(vec![Type::Note, Type::Read]),
        serde_qs::from_str::<Query>(
            "q=source&post-type[]=note&post-type[]=read&channel=jump&limit=5"
        )
        .map_err(|e| e.to_string())
        .map(|q| q.kind)
        .map(|k| match k {
            QueryKind::Source(query) => query.post_type,
            _ => vec![],
        }),
        "provides deserialization of the source query with multiple post type asked for"
    );
    assert_eq!(
        Ok(vec![Type::Video, Type::Read]),
        serde_qs::Config::new(1, false)
            .deserialize_str::<Query>(
                "q=source&post-type%5B0%5D=video&post-type%5B1%5D=read&limit=5"
            )
            .map_err(|e| e.to_string())
            .map(|q| q.kind)
            .map(|k| match k {
                QueryKind::Source(query) => query.post_type,
                _ => vec![],
            }),
        "provides deserialization of the source query with multiple post type asked for"
    );

    assert_eq!(
        None,
        serde_qs::from_str::<Query>("q=source&post-type=")
            .err()
            .map(|e| e.to_string()),
        "ignores if the post type value is 'empty'"
    );

    assert_eq!(
        None,
        serde_qs::from_str::<Query>("q=source&syndicate-to=3")
            .err()
            .map(|e| e.to_string()),
        "supports filtering by syndication targets"
    );
    assert_eq!(
        None,
        serde_qs::from_str::<Query>("q=config")
            .err()
            .map(|e| e.to_string()),
        "provides deserialization of the config query"
    );
}

#[test]
fn query_from_qs_str_with_property_filtering() {
    assert_eq!(
        Ok(QueryKind::Source(Box::new(SourceQuery {
            exists: vec!["in-reply-to".to_string()],
            not_exists: vec!["like-of".to_string()],
            matching_properties: MatchingPropertyValuesMap::from_iter(vec![
                ("byline".into(), vec!["today".into()]),
                ("range".into(), vec!["3".into(), "10".into()])
            ]),
            ..Default::default()
        }))),
        serde_qs::from_str::<Query>("q=source&not-exists=like-of&exists=in-reply-to&property-byline=today&property-range[]=3&property-range[]=10")
            .map(|q| q.kind)
            .map_err(|e| e.to_string()),
        "provides deserialization of the config query"
    );
}

#[test]
fn query_to_str() {
    let result1 = serde_qs::to_string(&QueryKind::Source(Box::new(SourceQuery {
        post_type: vec![Type::Article],
        ..Default::default()
    })));

    assert_eq!(
        result1.as_ref().err().map(|s| s.to_string()),
        None,
        "can query a list of articles"
    );
    assert_eq!(
        Some("q=source&post-type=article".to_string()),
        result1.ok(),
        "can query a list of articles"
    );

    let result2 = serde_qs::to_string(&QueryKind::Source(Box::new(SourceQuery {
        post_type: vec![Type::Article, Type::Note],
        ..Default::default()
    })));
    assert_eq!(
        Some("q=source&post-type[0]=article&post-type[1]=note".to_string()),
        result2.ok(),
        "can query a list of articles and notes"
    );
}

#[test]
#[cfg(feature = "experimental_channels")]
fn query_response_for_channels() {
    use super::extension;

    assert_eq!(
        Some(Response::Channel(extension::channel::QueryResponse {
            channels: vec![extension::channel::Form::Expanded {
                uid: "magic".to_string(),
                name: "Magic".to_string(),
                properties: serde_json::json!({"grr": "bark"}).try_into().unwrap()
            }],
            paging: Default::default()
        })),
        serde_json::from_value(serde_json::json!({
            "channels": [{
                "uid": "magic",
                "name": "Magic",
                "grr": "bark"
            }]
        }))
        .ok()
    )
}

#[test]
fn query_response_for_configuration() {
    assert_eq!(
        Ok(Response::Configuration(ConfigurationResponse {
            q: vec!["channels".to_owned()],
            category: vec!["tag".into()],
            media_endpoint: None,
            post_types: vec![Type::Note],
            channels: Default::default(),
            syndicate_to: Default::default()
        })),
        serde_json::from_value(serde_json::json!({
            "q": ["channels"],
            "post-types": ["note"],
            "category": ["tag"]
        }))
        .map(Response::Configuration)
        .map_err(crate::Error::JSON)
    );
}

#[test]
#[cfg(feature = "experimental_syndication")]
fn query_response_for_configuration_with_syndication() {
    use crate::standards::micropub::extension;

    assert_eq!(
        Ok(Response::Configuration(ConfigurationResponse {
            q: vec!["channels".to_owned()],
            category: vec!["tag".into()],
            media_endpoint: None,
            post_types: vec![Type::Note],
            channels: Default::default(),
            syndicate_to: vec![extension::syndication::Target {
                uid: "magic".into(),
                name: "cookie".into(),
                ..Default::default()
            }]
        })),
        serde_json::from_value(serde_json::json!({
            "q": ["channels"],
            "post-types": ["note"],
            "category": ["tag"],
            "syndicate-to": [
                {
                    "uid": "magic",
                    "name": "cookie"
                }
            ]
        }))
        .map(Response::Configuration)
        .map_err(crate::Error::JSON)
    );
}

#[test]
#[cfg(feature = "experimental_channels")]
fn query_response_for_configuration_with_channels() {
    assert_eq!(
        Ok(Response::Configuration(ConfigurationResponse {
            q: vec!["channels".to_owned()],
            category: vec!["tag".into()],
            media_endpoint: None,
            post_types: vec![Type::Note],
            channels: Default::default(),
            syndicate_to: Default::default()
        })),
        serde_json::from_value(serde_json::json!({
            "q": ["channels"],
            "post-types": ["note"],
            "category": ["tag"]
        }))
        .map(Response::Configuration)
        .map_err(crate::Error::JSON)
    );
}

#[test]
fn query_response_for_categories() {
    assert_eq!(
        Ok(Response::Category(CategoryResponse {
            categories: vec!["jump".into(), "kick".into(), "spin".into()],
            pagination: Default::default()
        })),
        serde_json::from_value(serde_json::json!({
            "categories": ["jump", "kick", "spin"]
        }))
        .map_err(|e| format!("{:#?}", e)),
        "works without paging"
    );
}

#[test]
fn query_response_for_categories_with_paging() {
    use super::extension::Order;

    assert_eq!(
        Ok(CategoryResponse {
            categories: vec!["jump".into(), "kick".into(), "spin".into()],
            pagination: paging::Fields {
                paging: paging::Query {
                    order: Some(Order::Descending),
                    ..Default::default()
                },
                ..Default::default()
            }
        }),
        serde_json::from_value(serde_json::json!({
            "categories": ["jump", "kick", "spin"], "paging": { "order": "desc" }
        }))
        .map_err(|e| format!("{:#?}", e)),
        "works with paging"
    )
}

#[test]
fn query_response_for_source() {
    let mut item = microformats_types::Item::default();
    item.r#type = vec![Class::Known(KnownClass::Entry)];
    assert_eq!(
        Ok(Response::Source(SourceResponse {
            post_type: vec![],
            item
        })),
        serde_json::from_value(serde_json::json!({
            "type": ["h-entry"],
            "properties": {}
        }))
        .map_err(|e| e.to_string())
    );

    assert_eq!(
        serde_json::from_value::<Response>(serde_json::json!(
        {
            "post-type": [
                "article"
            ],
            "properties": {
                "audience": [],
                "category": [],
                "channel": [
                    "all"
                ],
                "content": {
                    "html": "<p>well-here-we-go</p>"
                },
                "name": "magic-omg",
                "post-status": [
                    "published"
                ],
                "published": [
                    "2022-02-12T23:22:27+00:00"
                ],
                "slug": [
                    "Gzg043ii"
                ],
                "syndication": [],
                "updated": [
                    "2022-02-12T23:22:27+00:00"
                ],
                "url": [
                    "http://localhost:3112/Gzg043ii"
                ],
                "visibility": [
                    "public"
                ]
            },
            "type": [
                "h-entry"
            ]
        }
                ))
        .map_err(|e| e.to_string())
        .err(),
        None,
    )
}
