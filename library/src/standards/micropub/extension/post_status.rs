/// Represents the potential statuses a post can take.
///
/// More information about this can be found at <https://indieweb.org/post_status>.
#[derive(Debug, Clone, serde::Serialize, serde::Deserialize, PartialEq, Hash, Eq)]
#[serde(untagged, rename_all = "kebab-case")]
pub enum Status {
    /// The content is available for general use.
    Published,
    /// The content is not yet ready for general use.
    Drafted,
    /// The content has been considered "deleted".
    Deleted,
    /// The content has expired from general use.
    Expired,
    /// A custom status that posts can have.
    Other(String),
}

impl Default for Status {
    fn default() -> Self {
        Status::Published
    }
}

impl ToString for Status {
    fn to_string(&self) -> String {
        match self {
            Status::Published => "published".to_owned(),
            Status::Drafted => "draft".to_owned(),
            Status::Deleted => "deleted".to_owned(),
            Status::Expired => "expired".to_owned(),
            Status::Other(status) => status.to_owned(),
        }
    }
}

impl std::str::FromStr for Status {
    type Err = crate::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.is_empty() {
            // NOTE: This is the default publish state according to the spec.
            return Ok(Self::Published);
        }

        match s.to_lowercase().trim_matches('"') {
            "published" => Ok(Self::Published),
            "draft" => Ok(Self::Drafted),
            "deleted" => Ok(Self::Deleted),
            "expired" => Ok(Self::Expired),
            invalid => Err(Self::Err::InvalidPostStatus(invalid.to_string())),
        }
    }
}

#[test]
fn post_status() {
    use std::str::FromStr;
    assert_eq!(
        Some(Status::Drafted),
        Status::from_str("draft").ok()
    );
}
