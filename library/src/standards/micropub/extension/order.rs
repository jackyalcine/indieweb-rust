/// Provides representation of how content should be sorted in response to a query.
#[derive(Copy, Clone, Debug, PartialEq, serde::Serialize, serde::Deserialize, Hash, Eq)]
#[serde(try_from = "String", into = "String")]
pub enum Direction {
    Ascending,
    Descending,
}

impl std::str::FromStr for Direction {
    type Err = crate::Error;

    fn from_str(v: &str) -> Result<Self, Self::Err> {
        match v.to_lowercase().as_str() {
            "ascending" | "asc" | "a" => Ok(Self::Ascending),
            "descending" | "desc" | "d" => Ok(Self::Descending),
            i => Err(Self::Err::InvalidOrder(i.to_string())),
        }
    }
}

impl ToString for Direction {
    fn to_string(&self) -> String {
        match self {
            Self::Descending => "desc".to_owned(),
            Self::Ascending => "asc".to_owned(),
        }
    }
}

#[allow(clippy::from_over_into)]
impl Into<String> for Direction {
    fn into(self) -> String {
        self.to_string()
    }
}

impl TryFrom<String> for Direction {
    type Error = crate::Error;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        use std::str::FromStr;
        Self::from_str(&value)
    }
}
