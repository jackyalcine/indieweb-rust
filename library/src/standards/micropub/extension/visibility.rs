use std::str::FromStr;

use serde::{Deserialize, Serialize};

/// Provides representation of what content visibility can be shown as.
#[derive(Copy, Clone, Debug, PartialEq, Hash, Eq)]
pub enum Level {
    /// No restrictions on who can see this content.
    Public,

    /// Same as public, but hidden from associated feeds.
    Unlisted,

    /// Content can live in a feed but may require more information to view.
    Private,
}

impl Default for Level {
    fn default() -> Level {
        Level::Public
    }
}

impl FromStr for Level {
    type Err = crate::Error;

    fn from_str(v: &str) -> Result<Self, Self::Err> {
        match v.to_lowercase().as_str() {
            "private" => Ok(Level::Private),
            "unlisted" => Ok(Level::Unlisted),
            "public" => Ok(Level::Public),
            invalid => Err(Self::Err::InvalidVisibility(invalid.to_string())),
        }
    }
}

impl ToString for Level {
    fn to_string(&self) -> String {
        match self {
            Level::Public => "public".to_string(),
            Level::Unlisted => "unlisted".to_string(),
            Level::Private => "private".to_string(),
        }
    }
}

impl Serialize for Level {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        self.to_string().serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for Level {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        String::deserialize(deserializer)
            .and_then(|vs| Self::from_str(&vs).map_err(serde::de::Error::custom))
    }
}
