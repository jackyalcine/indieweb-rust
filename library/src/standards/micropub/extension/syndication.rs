use serde::{Deserialize, Serialize};

/// Provides a representation of a syndication-adjacent resource.
///
/// This can be used to describe a [target][SyndicationTarget] or
/// a profile of a target. This is defined in the Micropub spec at
/// <https://micropub.spec.indieweb.org/#syndication-targets-p-10> and
/// <https://micropub.spec.indieweb.org/#syndication-targets-p-11>.
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq, Default)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct Info {
    /// The name of the resource.
    pub name: String,

    /// The URL of the associated resource.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub url: Option<url::Url>,

    /// A URL meant to represent the associated resource.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub photo: Option<String>,
}

/// Provides a representation of a syndication target.
///
/// This is defined by <https://micropub.spec.indieweb.org/#syndication-targets-p-5>.
#[derive(Deserialize, Serialize, Clone, Debug, PartialEq, Default)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct Target {
    /// The UID representing this target.
    pub uid: String,

    /// A name associated to this target.
    #[serde(default)]
    pub name: String,

    /// Optional information relating to the service.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub service: Option<Info>,

    /// Optional information relating to the user.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub user: Option<Info>,
}
