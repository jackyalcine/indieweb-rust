pub use ::http::{Request, Response};
use http::{header::CONTENT_TYPE, HeaderValue};
use microformats_types::Document;

/// Represents the value for HTTP headers to show a form-encoded response.
pub static CONTENT_TYPE_FORM_URLENCODED: &str = "application/x-www-form-urlencoded";

/// Represents the value for HTTP headers to show a JSON-encoded response.
pub static CONTENT_TYPE_JSON: &str = "application/json";

#[derive(Clone)]
pub enum Body {
    Bytes(Vec<u8>),
    None,
}

impl std::fmt::Debug for Body {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Bytes(bytes) => f.debug_tuple("Bytes").field(&bytes.len()).finish(),
            Self::None => write!(f, "no bytes"),
        }
    }
}

impl std::default::Default for Body {
    fn default() -> Self {
        Self::None
    }
}

impl From<Vec<u8>> for Body {
    fn from(value: Vec<u8>) -> Self {
        Self::Bytes(value)
    }
}

impl From<String> for Body {
    fn from(value: String) -> Self {
        value.into_bytes().into()
    }
}

impl Body {
    pub fn as_bytes(&self) -> &[u8] {
        match self {
            Self::None => &[],
            Self::Bytes(b) => b,
        }
    }

    pub fn text(&self) -> Result<String, crate::Error> {
        String::from_utf8(self.as_bytes().to_vec()).map_err(crate::Error::FromUTF8)
    }
}

#[async_trait::async_trait]
pub trait Client: Send + Sync {
    async fn send_request(&self, request: Request<Body>) -> Result<Response<Body>, crate::Error>;
}

#[cfg(feature = "reqwest")]
pub mod reqwest {
    use std::time::Duration;

    use futures::TryFutureExt as _;

    fn into_local_request(
        request: ::http::Request<super::Body>,
    ) -> Result<reqwest::Request, crate::Error> {
        let method = reqwest::Method::from_bytes(request.method().as_str().as_bytes())
            .map_err(crate::Error::ReqwestMethod)?;
        let url = request.uri().to_string().parse()?;
        let mut req = reqwest::Request::new(method, url);

        for (name, value) in request.headers() {
            req.headers_mut().insert(name, value.clone());
        }

        let _ = req
            .body_mut()
            .insert(request.into_body().as_bytes().to_vec().into());

        Ok(req)
    }

    async fn into_local_response(
        response: reqwest::Response,
    ) -> Result<::http::Response<super::Body>, crate::Error> {
        let mut resp = ::http::Response::builder().status(response.status().as_u16());

        for (name, value) in response.headers() {
            resp = resp.header(name, value);
        }

        let body = response
            .bytes()
            .await
            .map_err(crate::Error::Reqwest)?
            .to_vec()
            .into();
        resp.body(body).map_err(crate::Error::Http)
    }

    pub struct Client(::reqwest::Client);

    impl std::default::Default for Client {
        fn default() -> Self {
            Self(
                ::reqwest::Client::builder()
                    .timeout(Duration::from_secs(5))
                    .build()
                    .expect("failed to build a http client"),
            )
        }
    }

    impl From<::reqwest::Client> for Client {
        fn from(client: ::reqwest::Client) -> Self {
            Self(client)
        }
    }

    #[async_trait::async_trait]
    impl super::Client for Client {
        #[tracing::instrument(skip(self))]
        async fn send_request(
            &self,
            request: ::http::Request<super::Body>,
        ) -> Result<::http::Response<super::Body>, crate::Error> {
            self.0
                .execute(into_local_request(request)?)
                .map_err(crate::Error::Reqwest)
                .and_then(|r| into_local_response(r))
                .await
        }
    }

    #[cfg(feature = "reqwest_middleware")]
    pub struct MiddlewareClient(::reqwest_middleware::ClientWithMiddleware);

    #[cfg(feature = "reqwest_middleware")]
    impl From<::reqwest_middleware::ClientWithMiddleware> for MiddlewareClient {
        fn from(client: ::reqwest_middleware::ClientWithMiddleware) -> Self {
            Self(client)
        }
    }

    #[cfg(feature = "reqwest_middleware")]
    #[async_trait::async_trait]
    impl super::Client for MiddlewareClient {
        #[tracing::instrument(skip(self))]
        async fn send_request(
            &self,
            request: ::http::Request<super::Body>,
        ) -> Result<::http::Response<super::Body>, crate::Error> {
            self.0
                .execute(into_local_request(request)?)
                .map_err(crate::Error::ReqwestMiddleware)
                .and_then(|r| into_local_response(r))
                .await
        }
    }
}

/// Converts [this response][::http::Response] into a [MF2 document][microformats_types::Document].
///
/// # Errors
///
/// This function will return an error if it could not detect the content type of this request or
/// fails to convert it into a MF2 document.
#[tracing::instrument(skip(resp))]
pub fn to_mf2_document(resp: Response<Body>, page_url: &str) -> Result<Document, crate::Error> {
    if resp.status().is_server_error() {
        return Err(crate::Error::RemoteServerFailure(resp.status()));
    };

    let ct_header = resp
        .headers()
        .get(CONTENT_TYPE)
        .cloned()
        .unwrap_or_else(|| HeaderValue::from_static("text/html"))
        .as_ref()
        .to_vec();

    let ct_header_str = String::from_utf8(ct_header)?;

    tracing::trace!(header = ct_header_str, "Resolved the remote content type.");

    if ct_header_str.starts_with("text/html") || ct_header_str.starts_with("text/mf2+html") {
        to_string(resp).and_then(|str| {
            microformats::from_html(&str, page_url.parse()?).map_err(crate::Error::Microformats)
        })
    } else if ct_header_str.starts_with("application/mf2+json") {
        to_json(resp).and_then(|v| Ok(serde_json::from_value(v)?))
    } else {
        Err(crate::Error::InvalidDocumentType(ct_header_str))
    }
}

#[tracing::instrument(skip(resp))]
pub fn to_string(resp: Response<Body>) -> Result<String, crate::Error> {
    if resp.status().is_server_error() {
        return Err(crate::Error::RemoteServerFailure(resp.status()));
    };
    Ok(String::from_utf8(resp.into_body().as_bytes().to_vec())?)
}

#[tracing::instrument(skip(resp))]
pub fn to_json(resp: Response<Body>) -> Result<serde_json::Value, crate::Error> {
    if resp.status().is_server_error() {
        return Err(crate::Error::RemoteServerFailure(resp.status()));
    };
    Ok(serde_json::from_slice(resp.into_body().as_bytes())?)
}

#[tracing::instrument(skip(resp))]
pub fn from_json_value<Value>(resp: Response<Body>) -> Result<Value, crate::Error>
where
    Value: serde::de::DeserializeOwned,
{
    to_json(resp).and_then(|v| serde_json::from_value(v).map_err(crate::Error::JSON))
}
