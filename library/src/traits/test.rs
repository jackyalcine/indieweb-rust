#![cfg(test)]

use url::Url;

use crate::traits::FetchMF2FromExt;

#[tracing_test::traced_test]
#[tokio::test]
async fn fetch_mf2_from_ext_url() {
    let mut client = crate::test::Client::new().await;
    let u: Url = format!("{}/test-endpoint-fetch", client.mock_server.url())
        .parse()
        .unwrap();
    let _ = client
        .mock_server
        .mock("GET", u.path())
        .with_status(200)
        .with_header("content-type", "text/html")
        .with_body(
            r#"
            <html>
                <body>
                    <main class="h-entry">
                        <a href="" class="u-url u-uid"></a>
                    </main>
                </body>
            </html>
            "#,
        )
        .expect_at_most(1)
        .create();
    let result = u.fetch_mf2_from(client).await;

    assert!(result.is_ok());
    assert_eq!(result.map(|doc| doc.items.len()), Ok(1));
}
