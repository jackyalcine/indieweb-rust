use http::StatusCode;

/// Represents all of the error states of this crate.
#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("No {rel:?} endpoints were found at {url:?}")]
    NoEndpointsFound { rel: String, url: String },

    #[error("The Webmention endpoint returned a unexpected status code of {0:?})")]
    WebmentionUnsupportedStatusCode(u16),

    #[error("No Webmention endpoint was provided.")]
    NoWebmentionEndpointProvided,

    #[error("The scopes {0:?} were missing in the provided list of scopes.")]
    MissingScope(crate::standards::indieauth::Scopes),

    #[error("IndieAuth error: {0}")]
    IndieAuth(#[from] crate::standards::indieauth::Error),

    #[error("Failed to build a CSS selector for {0}")]
    SelectorCompileFailure(String),

    #[error(transparent)]
    Io(#[from] std::io::Error),

    #[cfg(feature = "reqwest")]
    #[error(transparent)]
    Reqwest(#[from] reqwest::Error),

    #[cfg(feature = "reqwest")]
    #[error(transparent)]
    ReqwestMethod(#[from] http::method::InvalidMethod),

    #[cfg(feature = "reqwest")]
    #[error(transparent)]
    ReqwestMiddleware(#[from] reqwest_middleware::Error),

    #[error(transparent)]
    JSON(#[from] serde_json::Error),

    #[error(transparent)]
    FromUTF8(#[from] std::string::FromUtf8Error),
    #[error(transparent)]
    Other(#[from] anyhow::Error),

    #[error(transparent)]
    Url(#[from] url::ParseError),

    #[error(transparent)]
    Qs(#[from] serde_qs::Error),

    #[error("The value {0} is not recognized as a valid 'order'.")]
    InvalidOrder(String),

    #[error("The value {0} is not recognized as a valid status for posts.")]
    InvalidPostStatus(String),

    #[error("The value {0} is not recognized as a known visibility of posts.")]
    InvalidVisibility(String),

    #[error("No representative h-card could be found at {0}")]
    NoRepresentativeHCardFound(url::Url),

    #[error(transparent)]
    Micropub(#[from] crate::standards::micropub::Error),

    #[error(transparent)]
    Http(#[from] ::http::Error),

    #[error("Failed to parse the content {0} as a Microformats2 document.")]
    InvalidDocumentType(String),

    #[error("Got {0} when trying to read a remote resource.")]
    RemoteServerFailure(StatusCode),

    #[error(transparent)]
    Microformats(#[from] microformats::Error),
}

impl PartialEq for Error {
    fn eq(&self, other: &Self) -> bool {
        std::mem::discriminant(self) == std::mem::discriminant(other)
    }
}
