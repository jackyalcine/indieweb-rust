use std::{
    collections::{HashMap, HashSet},
    iter::FromIterator,
    str::FromStr,
};

use microformats_types::{Class, KnownClass};

/// A canonical list of the recognized post types.
///
/// A full list of them can be found at <https://indieweb.org/posts#Types_of_Posts>
// FIXME: Move 'experimental' types into a separate enum.
#[derive(
    Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Deserialize, serde::Serialize,
)]
#[serde(rename_all = "kebab-case")]
pub enum Type {
    /// <https://indieweb.org/like>
    Like,
    /// <https://indieweb.org/bookmark>
    Bookmark,
    /// <https://indieweb.org/reply>
    Reply,
    #[cfg(feature = "reaction")]
    /// <https://indieweb.org/reacji>
    Reaction,
    /// <https://indieweb.org/repost>
    Repost,
    /// <https://indieweb.org/note>
    Note,
    /// <https://indieweb.org/article>
    Article,
    /// <https://indieweb.org/photo>
    Photo,
    /// <https://indieweb.org/video>
    Video,
    /// <https://indieweb.org/audio>
    Audio,
    /// A catch-all type for more than one of a [Type::Photo], [Type::Audio] or [Type::Video] in one post.
    Media,
    /// <https://indieweb.org/quote>
    Quotation,
    /// <https://indieweb.org/gameplay>
    #[serde(rename = "gameplay")]
    GamePlay,
    /// <https://indieweb.org/rsvp>
    #[serde(rename = "rsvp")]
    RSVP,
    /// <https://indieweb.org/checkin>
    #[serde(rename = "checkin")]
    CheckIn,
    /// <https://indieweb.org/listen>
    Listen,
    /// <https://indieweb.org/watch>
    Watch,
    /// <https://indieweb.org/review>
    Review,
    /// <https://indieweb.org/read>
    Read,
    /// <https://indieweb.org/jam>
    Jam,
    /// <https://indieweb.org/follow>
    Follow,
    /// <https://indieweb.org/event>
    Event,
    /// <https://indieweb.org/issue>
    Issue,
    /// <https://indieweb.org/venue>
    Venue,
    /// <https://indieweb.org/collection>
    Collection,
    /// <https://indieweb.org/presentation>
    Presentation,
    /// <https://indieweb.org/exericse>
    Exercise,
    /// <https://indieweb.org/recipe>
    Recipe,
    /// <https://indieweb.org/wish>
    Wish,
    /// <https://indieweb.org/edit>
    Edit,
    /// <https://indieweb.org/sleep>
    Sleep,
    /// <https://indieweb.org/session>
    Session,
    /// <https://indieweb.org/snark>
    Snark,
    /// <https://indieweb.org/donation>
    Donation,
    /// <https://indieweb.org/want>
    Want,
    /// <https://indieweb.org/mention>
    Mention,
    /// An unknown and unrecognized post type.
    Other(String),
}

impl Default for Type {
    fn default() -> Type {
        Type::Note
    }
}

impl ToString for Type {
    fn to_string(&self) -> String {
        if let Self::Other(s) = self {
            s.to_string()
        } else {
            serde_json::to_value(self)
                .and_then(|v| serde_json::to_string(&v))
                .map(|s| s.trim_matches('"').to_string())
                .unwrap_or_else(|_| "other".to_string())
        }
    }
}

impl FromStr for Type {
    type Err = std::convert::Infallible;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(serde_json::from_str(&format!("\"{}\"", s))
            .unwrap_or_else(|_| Type::Other(s.to_string())))
    }
}

#[test]
fn type_to_string() {
    assert_eq!(Type::Note.to_string(), "note");
    assert_eq!(Type::Mention.to_string(), "mention");
    assert_eq!(Type::RSVP.to_string(), "rsvp");
    assert_eq!(Type::CheckIn.to_string(), "checkin");
    assert_eq!(Type::GamePlay.to_string(), "gameplay");
    assert_eq!(Type::Other("magic".to_string()).to_string(), "magic");
}

/// Represents the potential forms of defining a post type. The similar form
/// as represented by a single string is the one conventionally used. The
/// expanded form is one that's being experimented on to allow for the definition
/// of the constraints that a type can set.
#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
#[serde(untagged, rename_all = "kebab-case")]
pub enum PostType {
    /// Represents a simpler (textual) form of a post type.
    Simple(Type),

    /// Represents an expanded way to describe a post type.
    Expanded {
        /// The presentational name of the post type.
        name: String,

        /// The known post type being expanded.
        #[serde(rename = "type")]
        kind: Type,

        /// The container type represented as a [microformats_types::Class]
        #[serde(default = "default_class")]
        h: Class,

        /// Recognized properties for this post type.
        #[serde(default)]
        properties: Vec<String>,

        /// Properties for this post type that are required for it to be defined as this post type.
        #[serde(default)]
        required_properties: Vec<String>,
    },
}

fn default_class() -> Class {
    Class::Known(KnownClass::Entry)
}

impl From<PostType> for Type {
    fn from(post_type: PostType) -> Type {
        match post_type {
            PostType::Simple(kind) => kind,
            PostType::Expanded { kind, .. } => kind,
        }
    }
}

impl From<&PostType> for Type {
    fn from(post_type: &PostType) -> Type {
        match post_type {
            PostType::Simple(kind) => kind.clone(),
            PostType::Expanded { kind, .. } => kind.clone(),
        }
    }
}

impl PartialEq for PostType {
    fn eq(&self, other: &Self) -> bool {
        let ltype: Type = self.into();
        let rtype: Type = other.into();

        ltype == rtype
    }
}

impl PostType {
    /// Provides a human-friendly descriptor of this post type.
    pub fn name(&self) -> String {
        match self {
            Self::Simple(simple_type) => simple_type.to_string(),
            Self::Expanded { ref name, .. } => name.to_string(),
        }
    }

    /// Provides the actual type represented.
    pub fn kind(&self) -> String {
        match self {
            Self::Simple(simple_type) => simple_type.to_string(),
            Self::Expanded { ref kind, .. } => kind.to_string(),
        }
    }
}

#[test]
fn post_type_name() {
    assert_eq!(PostType::Simple(Type::Note).name(), "note".to_string());
    assert_eq!(PostType::Simple(Type::RSVP).name(), "rsvp".to_string());
}

fn properties_from_type() -> HashMap<String, Type> {
    HashMap::from_iter(
        vec![
            ("like-of".to_owned(), Type::Like),
            ("in-reply-to".to_owned(), Type::Reply),
            ("bookmark-of".to_owned(), Type::Bookmark),
            ("repost-of".to_owned(), Type::Repost),
            ("quotation-of".to_owned(), Type::Quotation),
            ("gameplay-of".to_owned(), Type::GamePlay),
            ("follow-of".to_owned(), Type::Follow),
            ("jam-of".to_owned(), Type::Jam),
            ("listen-of".to_owned(), Type::Listen),
            ("rsvp".to_owned(), Type::RSVP),
            ("photo".to_owned(), Type::Photo),
            ("video".to_owned(), Type::Video),
            ("audio".to_owned(), Type::Audio),
            ("checkin".to_owned(), Type::CheckIn),
            ("read-of".to_owned(), Type::Read),
            ("media".to_owned(), Type::Media),
            ("mention-of".to_owned(), Type::Mention),
        ]
        .iter()
        .cloned(),
    )
}

static REPLY_CONTEXT_PROPERTIES: [&str; 12] = [
    "in-reply-to",
    "like-of",
    "bookmark-of",
    "repost-of",
    "quotation-of",
    "follow-of",
    "listen-of",
    "gameplay-of",
    "mention-of",
    "rsvp",
    "read-of",
    "checkin",
];

/// Determines if the provided property is one that implies a contextual response.
///
/// This is mainly opinionated so please send PRs for property names you'd like to see here.
///
/// # Examples
/// ```
/// # use indieweb::algorithms::ptd::is_reply_context_property;
/// assert!(is_reply_context_property("read-of"), "'read-of' is a reaction to reading something");
/// assert!(!is_reply_context_property("content"), "'content' does not indicate a reaction to anything");
/// ```
pub fn is_reply_context_property(property_name: &str) -> bool {
    REPLY_CONTEXT_PROPERTIES.contains(&property_name)
}

/// Determines the type of reactionary post this is from the provided property names.
///
/// See [resolve_from_property_names] for more information.
pub fn resolve_reaction_property_name(property_names: &[&str]) -> Option<Type> {
    let hashed = REPLY_CONTEXT_PROPERTIES
        .iter()
        .map(|s| s.to_string())
        .collect::<HashSet<String>>();
    let got = property_names.iter().map(|s| s.to_string()).collect();
    let mut reaction_types = hashed.intersection(&got).cloned().collect::<Vec<_>>();
    reaction_types.sort();
    reaction_types.dedup();

    resolve_from_property_names(reaction_types)
        .filter(|v| !matches!(v, Type::Note) || !matches!(v, Type::Article))
        .or(Some(Type::Mention))
}

#[test]
fn resolve_reaction_property_name_test() {
    assert_eq!(
        resolve_reaction_property_name(&["content", "in-reply-to"]),
        Some(Type::Reply)
    );
    assert_eq!(
        resolve_reaction_property_name(&["url", "gameplay-of"]),
        Some(Type::GamePlay)
    );
}

// FIXME: Figure out how to consider RSVPs.
// FIXME: Should reacjis be different here.
/// Determines the property name to use for publishing for a post type.
///
/// This is useful for semantically organizing things like Webmentions
/// or items in a collection of posts (or feed).
pub fn type_to_reaction_property_name(t: Type) -> &'static str {
    match t {
        Type::Reply => "comment",
        #[cfg(feature = "reaction")]
        Type::Reaction => "reaction",
        Type::Like => "like",
        Type::Bookmark => "bookmark",
        Type::Repost => "repost",
        _ => "mention",
    }
}

#[test]
fn type_to_reaction_property_name_test() {
    assert_eq!(type_to_reaction_property_name(Type::Like), "like");
    #[cfg(feature = "reaction")]
    assert_eq!(type_to_reaction_property_name(Type::Reaction), "reaction");
    assert_eq!(type_to_reaction_property_name(Type::Reply), "comment");
    assert_eq!(type_to_reaction_property_name(Type::Note), "mention");
    assert_eq!(type_to_reaction_property_name(Type::Article), "mention");
    assert_eq!(
        type_to_reaction_property_name(Type::Other("grr".to_string())),
        "mention"
    );
}

/// Aims to resolve the known post type of the provided [microformats_types::Item].
pub fn resolve_from_object<V>(into_item_mf2: V) -> Option<Type>
where
    V: TryInto<microformats_types::Item>,
{
    if let Ok(item_mf2) = into_item_mf2.try_into() {
        let keys = item_mf2
            .properties
            .keys()
            .map(|s| s.to_owned())
            .collect::<Vec<_>>();

        resolve_from_property_names(keys).map(|post_type| {
            #[cfg(feature = "reaction")]
            if post_type == Type::Reply && has_reaction_emoji_as_content(item_mf2) {
                return Type::Reaction;
            }

            post_type
        })
    } else {
        None
    }
}

#[cfg(feature = "reaction")]
fn has_reaction_emoji_as_content<V>(into_item_mf2: V) -> bool
where
    V: TryInto<microformats_types::Item>,
{
    use microformats_types::{Fragment, PropertyValue};
    if let Ok(contents) = into_item_mf2.try_into().map(|item| item.content()) {
        contents
            .unwrap_or_default()
            .into_iter()
            .any(|content_value| match content_value {
                PropertyValue::Plain(text)
                | PropertyValue::Fragment(Fragment { value: text, .. }) => {
                    let emojis = emojito::find_emoji(text.clone());

                    !emojis.is_empty()
                        && emojis.iter().map(|e| e.glyph).collect::<Vec<_>>().join("") == text
                }
                PropertyValue::Item(item) => has_reaction_emoji_as_content(item),
                _ => false,
            })
    } else {
        false
    }
}

/// Determines a potential post type from a list of property names.
///
/// # Examples
/// ```
/// # use indieweb::algorithms::ptd::*;
/// assert_eq!(
///     resolve_from_property_names(vec!["like-of".into()]),
///     Some(Type::Like),
///     "'like-of' indicates a like post.");
///
/// assert_eq!(
///     resolve_from_property_names(vec!["content".into()]),
///     Some(Type::Note),
///     "Just 'content' is a note.");
/// ```
pub fn resolve_from_property_names(names: Vec<String>) -> Option<Type> {
    let has_content = names.contains(&"content".to_owned());
    let has_name = names.contains(&"name".to_string());

    let mut types: Vec<Type> = vec![];

    properties_from_type().iter().for_each(|(key, val)| {
        if names.contains(key) {
            types.push(val.clone());
        }
    });

    if has_name && has_content {
        types.push(Type::Article)
    } else if !has_name && has_content {
        types.push(Type::Note)
    }

    let first_type = types.first().cloned();

    combinatory_type(types.clone())
        .or(first_type)
        .or(Some(Type::Note))
}

fn combinatory_type(types: Vec<Type>) -> Option<Type> {
    [
        (Type::RSVP, vec![Type::Reply, Type::RSVP]),
        (Type::Photo, vec![Type::Photo, Type::Note]),
        (Type::Video, vec![Type::Video, Type::Photo, Type::Note]),
        (
            Type::Media,
            vec![Type::Audio, Type::Video, Type::Photo, Type::Note],
        ),
    ]
    .iter()
    .find_map(|(combined_type, expected_types)| {
        if expected_types
            .iter()
            .all(|post_type| types.contains(post_type))
        {
            Some(combined_type.to_owned())
        } else {
            None
        }
    })
}

#[test]
fn post_type_from_json() {
    assert_eq!(
        serde_json::from_str::<PostType>(
            r#"
                {
                    "name": "Note",
                    "type": "note"
                }
                "#
        )
        .ok(),
        Some(PostType::Expanded {
            name: "Note".to_string(),
            kind: Type::Note,
            h: default_class(),
            properties: Vec::default(),
            required_properties: Vec::default()
        })
    );

    assert_eq!(
        serde_json::from_str::<Vec<PostType>>(
            r#"
                [{
                    "name": "Note",
                    "type": "note"
                }, "like"]
                "#
        )
        .ok(),
        Some(vec![
            PostType::Expanded {
                name: "Note".to_string(),
                kind: Type::Note,
                h: default_class(),
                properties: Vec::default(),
                required_properties: Vec::default()
            },
            PostType::Simple(Type::Like)
        ])
    );

    assert_eq!(
        serde_json::from_str::<PostType>(r#""note""#).ok(),
        Some(PostType::Simple(Type::Note))
    );

    assert_eq!(
        serde_qs::from_str::<Type>("note").map_err(|e| e.to_string()),
        Ok(Type::Note)
    );

    #[derive(serde::Deserialize, PartialEq, Debug)]
    struct V {
        v: Vec<Type>,
    }

    assert_eq!(
        serde_qs::from_str::<V>("v[0]=note&v[1]=like").map_err(|e| e.to_string()),
        Ok(V {
            v: vec![Type::Note, Type::Like]
        })
    );
}

#[test]
fn type_from_str() {
    assert_eq!(Type::from_str("note"), Ok(Type::Note));
}

#[test]
fn post_type_partial_eq() {
    assert_eq!(
        PostType::Simple(Type::Like),
        PostType::Expanded {
            kind: Type::Like,
            name: "Like".to_string(),
            h: default_class(),
            properties: Vec::default(),
            required_properties: Vec::default()
        }
    );
}

#[test]
fn resolve_from_object_test() {
    assert_eq!(
        resolve_from_object(serde_json::json!({
            "type": ["h-entry"],
            "properties": {
                "like-of": ["https://indieweb.org/like"]
            }
        })),
        Some(Type::Like)
    );
    assert_eq!(
        resolve_from_object(serde_json::json!({
            "type": ["h-entry"],
            "properties": {
                "in-reply-to": ["https://indieweb.org/rsvp"],
                "rsvp": ["yes"]
            }
        })),
        Some(Type::RSVP)
    );
    #[cfg(feature = "reaction")]
    assert_eq!(
        resolve_from_object(serde_json::json!({
            "type": ["h-entry"],
            "properties": {
                "in-reply-to": ["https://indieweb.org/rsvp"],
                "content": "🚀"
            }
        })),
        Some(Type::Reaction)
    );
    #[cfg(feature = "reaction")]
    assert_eq!(
        resolve_from_object(serde_json::json!({
            "type": ["h-entry"],
            "properties": {
                "in-reply-to": ["https://indieweb.org/rsvp"],
                "content": ["👋🏿"]
            }
        })),
        Some(Type::Reaction)
    );
    assert_eq!(
        resolve_from_object(serde_json::json!({
            "type": ["h-entry"],
            "properties": {
                "in-reply-to": ["https://indieweb.org/rsvp"],
                "content": ["hey there! 👋🏿"]
            }
        })),
        Some(Type::Reply)
    );
}

#[test]
fn resolve_from_property_names_test() {
    assert_eq!(
        resolve_from_property_names(vec!["like-of".into()]),
        Some(Type::Like)
    );
}
