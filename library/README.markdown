# IndieWeb for Rust

This library provides a collection of tools to work with the IndieWeb
in your Rust applications.

## Features

- [IndieAuth][] supporting <https://indieauth.spec.indieweb.org/20201126/>
- [Webmention][] support <https://www.w3.org/TR/webmention/>
