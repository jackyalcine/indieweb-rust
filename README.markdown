# `indieweb`

[![Build Status for 'main'](https://ci.jacky.wtf/api/badges/indieweb/rust/status.svg?ref=refs/heads/main)](https://ci.jacky.wtf/indieweb/rust)
[![Build Status for 'next'](https://ci.jacky.wtf/api/badges/indieweb/rust/status.svg?ref=refs/heads/next)](https://ci.jacky.wtf/indieweb/rust)

This is a crate holding a collection of utilities that other libraries
and applications alike can use to interact with IndieWeb services like
one's site or a service that provides a IndieWeb-compatible system.

## Goals

- [ ] HTTP client agnostic
- [ ] Normalized interactions with IndieWeb services
- [ ] Errors for most known usecases

## Features

- [link relationship resolution][linkrel]
- [Micropub][]
- [Microsub][]
- [IndieAuth][]
- [WebSub][]
- [Webmention][]

[Micropub]: https://micropub.spec.indieweb.org/
[Microsub]: http://microsub.spec.indieweb.org/
[IndieAuth]: https://indieauth.spec.indieweb.org/
[Webmention]: https://webmention.net/draft/
[WebSub]: https://www.w3.org/TR/websub/
[linkrel]: https://microformats.org/wiki/rel
